let dictionary = [
	{
		word: "a",
		usage: {
			particle: "(emphasis, emotion or confirmation)",
			synonym: "kin"
		}
	},
	{
		word: "akesi",
		usage: {
			noun: "non-cute animal; reptile, amphibian"
		}
	},
	{
		word: "ala",
		usage: {
			adjective: "no, not, zero"
		}
	},
	{
		word: "alasa",
		usage: {
			verb: "to hunt, forage"
		}
	},
	{
		word: "ale",
		usage: {
			adjective: "all; abundant, countless, bountiful, every, plentiful",
			noun: "abundance, everything, life, universe",
			number: "100",
			synonym: "ali"
		}
	},
	{
		word: "anpa",
		usage: {
			adjective: "bowing down, downward, humble, lowly, dependent"
		}
	},
	{
		word: "ante",
		usage: {
			adjective: "different, altered, changed, other"
		}
	},
	{
		word: "anu",
		usage: {
			particle: "or"
		}
	},
	{
		word: "awen",
		usage: {
			adjective: "enduring, kept, protected, safe, waiting, staying",
			preverb: "to continue to"
		}
	},
	{
		word: "e",
		usage: {
			particle: "(before the direct object)"
		}
	},
	{
		word: "en",
		usage: {
			particle: "(between multiple subjects)"
		}
	},
	{
		word: "esun",
		usage: {
			noun: "market, shop, fair, bazaar, business transaction"
		}
	},
	{
		word: "ijo",
		usage: {
			noun: "thing, phenomenon, object, matter"
		}
	},
	{
		word: "ike",
		usage: {
			adjective: "bad, negative; non-essential, irrelevant"
		}
	},
	{
		word: "ilo",
		usage: {
			noun: "tool, implement, machine, device"
		}
	},
	{
		word: "insa",
		usage: {
			noun: "centre, content, inside, between; internal organ, stomach"
		}
	},
	{
		word: "jaki",
		usage: {
			adjective: "disgusting, obscene, sickly, toxic, unclean, unsanitary"
		}
	},
	{
		word: "jan",
		usage: {
			noun: "human being, person, somebody"
		}
	},
	{
		word: "jelo",
		usage: {
			adjective: "yellow, yellowish"
		}
	},
	{
		word: "jo",
		usage: {
			verb: "to have, carry, contain, hold"
		}
	},
	{
		word: "kala",
		usage: {
			noun: "fish, marine animal, sea creature"
		}
	},
	{
		word: "kalama",
		usage: {
			verb: "to produce a sound; recite, utter aloud"
		}
	},
	{
		word: "kama",
		usage: {
			adjective: "arriving, coming, future, summoned",
			preverb: "to become, manage to, succeed in"
		}
	},
	{
		word: "kasi",
		usage: {
			noun: "plant, vegetation; herb, leaf"
		}
	},
	{
		word: "ken",
		usage: {
			preverb: "to be able to, be allowed to, can, may",
			adjective: "possible"
		}
	},
	{
		word: "kepeken",
		usage: {
			preposition: "to use, with, by means of"
		}
	},
	{
		word: "kili",
		usage: {
			noun: "fruit, vegetable, mushroom"
		}
	},
	{
		word: "kiwen",
		usage: {
			noun: "hard object, metal, rock, stone"
		}
	},
	{
		word: "ko",
		usage: {
			noun: "clay, clinging form, dough, semi-solid, paste, powder"
		}
	},
	{
		word: "kon",
		usage: {
			noun: "air, breath; essence, spirit; hidden reality, unseen agent"
		}
	},
	{
		word: "kule",
		usage: {
			adjective: "colourful, pigmented, painted"
		}
	},
	{
		word: "kulupu",
		usage: {
			noun: "community, company, group, nation, society, tribe"
		}
	},
	{
		word: "kute",
		usage: {
			noun: "ear",
			verb: "to hear, listen; pay attention to, obey"
		}
	},
	{
		word: "la",
		usage: {
			particle: "(between the context phrase and the main sentence)"
		}
	},
	{
		word: "lape",
		usage: {
			adjective: "sleeping, resting"
		}
	},
	{
		word: "laso",
		usage: {
			adjective: "blue, green"
		}
	},
	{
		word: "lawa",
		usage: {
			noun: "head, mind",
			verb: "to control, direct, guide, lead, own, plan, regulate, rule"
		}
	},
	{
		word: "len",
		usage: {
			noun: "cloth, clothing, fabric, textile; cover, layer of privacy"
		}
	},
	{
		word: "lete",
		usage: {
			adjective: "cold, cool; uncooked, raw"
		}
	},
	{
		word: "li",
		usage: {
			particle: "(between any subject except mi alone, sina alone and its VERB; also to introduce a new VERB for the same subject)"
		}
	},
	{
		word: "lili",
		usage: {
			adjective: "little, small, short; few; a bit; young"
		}
	},
	{
		word: "linja",
		usage: {
			noun: "long and flexible thing; cord, hair, rope, thread, yarn"
		}
	},
	{
		word: "lipu",
		usage: {
			noun: "flat object; book, document, card, paper, record, website"
		}
	},
	{
		word: "loje",
		usage: {
			adjective: "red, reddish"
		}
	},
	{
		word: "lon",
		usage: {
			preposition: "located at, present at, real, true, existing"
		}
	},
	{
		word: "luka",
		usage: {
			noun: "arm, hand, tactile organ",
			number: "five"
		}
	},
	{
		word: "lukin",
		usage: {
			noun: "eye",
			verb: "to look at, see, examine, observe, read, watch",
			preverb: "to seek, look for, try to",
			synonym: "oko"
		}
	},
	{
		word: "lupa",
		usage: {
			noun: "door, hole, orifice, window"
		}
	},
	{
		word: "ma",
		usage: {
			noun: "earth, land; outdoors, world; country, territory; soil"
		}
	},
	{
		word: "mama",
		usage: {
			noun: "parent, ancestor; creator, originator; caretaker, sustainer"
		}
	},
	{
		word: "mani",
		usage: {
			noun: "money, cash, savings, wealth; large domesticated animal"
		}
	},
	{
		word: "meli",
		usage: {
			noun: "woman, female, feminine person; wife"
		}
	},
	{
		word: "mi",
		usage: {
			noun: "I, me, we, us"
		}
	},
	{
		word: "mije",
		usage: {
			noun: "man, male, masculine person; husband"
		}
	},
	{
		word: "moku",
		usage: {
			verb: "to eat, drink, consume, swallow, ingest"
		}
	},
	{
		word: "moli",
		usage: {
			adjective: "dead, dying"
		}
	},
	{
		word: "monsi",
		usage: {
			noun: "back, behind, rear"
		}
	},
	{
		word: "mu",
		usage: {
			particle: "(animal noise or communication)"
		}
	},
	{
		word: "mun",
		usage: {
			noun: "moon, night sky object, star"
		}
	},
	{
		word: "musi",
		usage: {
			adjective: "artistic, entertaining, frivolous, playful, recreational"
		}
	},
	{
		word: "mute",
		usage: {
			adjective: "many, a lot, more, much, several, very",
			noun: "quantity"
		}
	},
	{
		word: "nanpa",
		usage: {
			particle: "-th (ordinal number)",
			noun: "numbers"
		}
	},
	{
		word: "nasa",
		usage: {
			adjective: "unusual, strange; foolish, crazy; drunk, intoxicated"
		}
	},
	{
		word: "nasin",
		usage: {
			noun: "way, custom, doctrine, method, path, road"
		}
	},
	{
		word: "nena",
		usage: {
			noun: "bump, button, hill, mountain, nose, protuberance"
		}
	},
	{
		word: "ni",
		usage: {
			adjective: "that, this"
		}
	},
	{
		word: "nimi",
		usage: {
			noun: "name, word"
		}
	},
	{
		word: "noka",
		usage: {
			noun: "foot, leg, organ of locomotion; bottom, lower part"
		}
	},
	{
		word: "o",
		usage: {
			particle: "(hey! O! (vocative or imperative)"
		}
	},
	{
		word: "olin",
		usage: {
			verb: "to love, have compassion for, respect, show affection to"
		}
	},
	{
		word: "ona",
		usage: {
			noun: "he, she, it, they"
		}
	},
	{
		word: "open",
		usage: {
			verb: "to begin, start; open; turn on"
		}
	},
	{
		word: "pakala",
		usage: {
			adjective: "botched, broken, damaged, harmed, messed up"
		}
	},
	{
		word: "pali",
		usage: {
			verb: "to do, take action on, work on; build, make, prepare"
		}
	},
	{
		word: "palisa",
		usage: {
			noun: "long hard thing; branch, rod, stick"
		}
	},
	{
		word: "pan",
		usage: {
			noun: "cereal, grain; barley, corn, oat, rice, wheat; bread, pasta"
		}
	},
	{
		word: "pana",
		usage: {
			verb: "to give, send, emit, provide, put, release"
		}
	},
	{
		word: "pi",
		usage: {
			particle: "of"
		}
	},
	{
		word: "pilin",
		usage: {
			noun: "(heart (physical or emotional)",
			adjective: "feeling (an emotion, a direct experience)"
		}
	},
	{
		word: "pimeja",
		usage: {
			adjective: "black, dark, unlit"
		}
	},
	{
		word: "pini",
		usage: {
			adjective: "ago, completed, ended, finished, past"
		}
	},
	{
		word: "pipi",
		usage: {
			noun: "bug, insect, ant, spider"
		}
	},
	{
		word: "poka",
		usage: {
			noun: "hip, side; next to, nearby, vicinity"
		}
	},
	{
		word: "poki",
		usage: {
			noun: "container, bag, bowl, box, cup, cupboard, drawer, vessel"
		}
	},
	{
		word: "pona",
		usage: {
			adjective: "good, positive, useful; friendly, peaceful; simple"
		}
	},
	{
		word: "pu",
		usage: {
			adjective: "interacting with the official Toki Pona book"
		}
	},
	{
		word: "sama",
		usage: {
			adjective: "same, similar; each other; sibling, peer, fellow",
			preposition: "as, like"
		}
	},
	{
		word: "seli",
		usage: {
			adjective: "fire; cooking element, chemical reaction, heat source"
		}
	},
	{
		word: "selo",
		usage: {
			noun: "outer form, outer layer; bark, peel, shell, skin; boundary"
		}
	},
	{
		word: "seme",
		usage: {
			particle: "what? which?"
		}
	},
	{
		word: "sewi",
		usage: {
			noun: "area above, highest part, something elevated",
			adjective: "awe-inspiring, divine, sacred, supernatural"
		}
	},
	{
		word: "sijelo",
		usage: {
			noun: "(body (of person or animal), physical state, torso"
		}
	},
	{
		word: "sike",
		usage: {
			noun: "round or circular thing; ball, circle, cycle, sphere, wheel",
			adjective: "of one year"
		}
	},
	{
		word: "sin",
		usage: {
			adjective: "new, fresh; additional, another, extra",
			synonym: "namako"
		}
	},
	{
		word: "sina",
		usage: {
			noun: "you"
		}
	},
	{
		word: "sinpin",
		usage: {
			noun: "face, foremost, front, wall"
		}
	},
	{
		word: "sitelen",
		usage: {
			noun: "image, picture, representation, symbol, mark, writing"
		}
	},
	{
		word: "sona",
		usage: {
			verb: "to know, be skilled in, be wise about, have information on",
			preverb: "to know how to"
		}
	},
	{
		word: "soweli",
		usage: {
			noun: "animal, beast, land mammal"
		}
	},
	{
		word: "suli",
		usage: {
			adjective: "big, heavy, large, long, tall; important; adult"
		}
	},
	{
		word: "suno",
		usage: {
			noun: "sun; light, brightness, glow, radiance, shine; light source"
		}
	},
	{
		word: "supa",
		usage: {
			noun: "horizontal surface, thing to put or rest something on"
		}
	},
	{
		word: "suwi",
		usage: {
			adjective: "sweet, fragrant; cute, innocent, adorable"
		}
	},
	{
		word: "tan",
		usage: {
			preposition: "by, from, because of"
		}
	},
	{
		word: "taso",
		usage: {
			particle: "but, however",
			adjective: "only"
		}
	},
	{
		word: "tawa",
		usage: {
			preposition: "going to, toward; for; from the perspective of",
			adjective: "moving"
		}
	},
	{
		word: "telo",
		usage: {
			noun: "water, liquid, fluid, wet substance; beverage"
		}
	},
	{
		word: "tenpo",
		usage: {
			noun: "time, duration, moment, occasion, period, situation"
		}
	},
	{
		word: "toki",
		usage: {
			verb: "to communicate, say, speak, say, talk, use language, think"
		}
	},
	{
		word: "tomo",
		usage: {
			noun: "indoor space; building, home, house, room"
		}
	},
	{
		word: "tu",
		usage: {
			number: "two"
		}
	},
	{
		word: "unpa",
		usage: {
			verb: "to have sexual or marital relations with"
		}
	},
	{
		word: "uta",
		usage: {
			noun: "mouth, lips, oral cavity, jaw"
		}
	},
	{
		word: "utala",
		usage: {
			verb: "to battle, challenge, compete against, struggle against"
		}
	},
	{
		word: "walo",
		usage: {
			adjective: "white, whitish; light-coloured, pale"
		}
	},
	{
		word: "wan",
		usage: {
			adjective: "unique, united",
			number: "one"
		}
	},
	{
		word: "waso",
		usage: {
			noun: "bird, flying creature, winged animal"
		}
	},
	{
		word: "wawa",
		usage: {
			adjective: "strong, powerful; confident, sure; energetic, intense"
		}
	},
	{
		word: "weka",
		usage: {
			adjective: "absent, away, ignored"
		}
	},
	{
		word: "wile",
		usage: {
			preverb: "must, need, require, should, want, wish"
		}
	}
];

export {dictionary};