import {dictionary as source} from './dictionary.js';
import {cookie} from './cookie.js';

class Dictionary {
    constructor(source) {
        this.source = source;
    }

    searchIn(searchOption) {
        return main.searchOptions.indexOf(searchOption) > -1;
    }

    find(word) {
        let found = [];
        let reg = new RegExp(word, 'i');
        
        for (let i = 0; i < this.source.length; i++) {
            if (this.searchIn('words') && this.source[i].word.match(reg)) {
                found.push(this.source[i]);
            }
            else if (
                this.searchIn('words')
                && typeof this.source[i].usage.synonym !== 'undefined' 
                && this.source[i].usage.synonym.match(reg)
            ) {
                found.push({
                    word: this.source[i].usage.synonym,
                    usage: this.source[i].usage,
                    synonym: this.source[i].word
                });
            }
            else {
                let usageFound = false;

                for (let j in this.source[i].usage) {
                    if (!this.source[i].usage.hasOwnProperty(j) || usageFound)
                        continue;
                    
                    if (this.searchIn('terms') && j.match(reg)) {
                        found.push(this.source[i]);
                        usageFound = true;
                    }
                    else if (
                        this.searchIn('definitions')
                        && word.length >= 2
                        && this.source[i].usage[j].match(reg)
                    ) {
                        found.push(this.source[i]);
                        usageFound = true;
                    }
                }
            }
        }

        if (!found.length && !word.length) {
            found = this.source;
        }

        return found.reduce((x, y) => {
            let w = new Elem('word', {
                word: y.word,
                usage: this.constructUsage(y.usage)
            });

            w.setId(y.word);

            x.push(w);
            return x;
        }, []);
    }

    constructUsage(usage) {
        let usages = [];
        for (let i in usage) {
            if (!usage.hasOwnProperty(i))
                continue;
            
            let u = new Elem('usage', {
                term: i,
                definition: usage[i],

                searchUsage: (ev, me) => {
                    app.props.words = dictionary.find(me.props.term);
                    app.set('search', me.props.term);
                }
            });

            u.setId(i);

            usages.push(u);
        }

        return usages;
    }
}

class Main {
    constructor(cookie) {
        this.searchBarOffset = null;
        this.searchOptions = cookie.exists('searchOptions')
            ? cookie.get('searchOptions').split(',')
            : ['words', 'terms', 'definitions'];
    }
    getTheme(value) {
        return parseInt(value) ? 'app--theme-dark' : 'app--theme-light';
    }
    stickySearch() {
        if (this.searchBarOffset === null) {
            this.searchBarOffset = document.querySelector('.search-bar').offsetTop;
        }

        if (window.pageYOffset >= this.searchBarOffset) {
            app.set('stickyClass', 'search-bar--sticky');
        }
        else {
            app.set('stickyClass', '');
        }
    }
}

let main = new Main(cookie);
let dictionary = new Dictionary(source);
let savedTheme = cookie.get('theme') || 1;

let flw = new Flw('.main');

let menu = new Elem('menu', {
    activeClass: '',
    icon: '☰',
    theme: parseInt(savedTheme),
    language: 'en',
    searchOptions: main.searchOptions,

    toggleMenu: (ev, me) => {
        if (me.props.activeClass.length) {
            me.props.activeClass = '';
            me.props.icon = '☰';
        }
        else {
            me.props.activeClass = 'menu--active';
            me.props.icon = '×';
        }

        me.renderer.update();
    },

    initSearch: (ev, me) => {
        if (ev.target.checked) {
            me.props.searchOptions.push(ev.target.value);
        }
        else {
            me.props.searchOptions.splice(me.props.searchOptions.indexOf(ev.target.value), 1);
        }

        cookie.set('searchOptions', me.props.searchOptions);

        main.searchOptions = me.props.searchOptions;
        
        document.querySelector('.search-bar__input').dispatchEvent(new Event('input'));
    },

    switchTheme: (ev, me) => {
        let themeVal = me.props.theme ? 0 : 1;
        me.props.theme = themeVal;
        app.set('themeClass', main.getTheme(themeVal));

        cookie.set('theme', themeVal);
    }
});

let app = new Elem('app', {
    themeClass: main.getTheme(savedTheme),
    nothingClass: 'hidden',
    search: '',
    menu: menu,
    words: dictionary.find(''),

    searchWord: (ev, me) => {
        let val = ev.target.value;
        let found = dictionary.find(val);

        me.props.nothingClass = found.length ? 'hidden' : '';
        me.props.search = val;
        me.set('words', found);
    },
});

flw.addElem(app, 'app');
flw.run();

if ('serviceWorker' in navigator) {
    navigator.serviceWorker.register('/sw.js');
}

window.onscroll = () => {
    main.stickySearch();
}