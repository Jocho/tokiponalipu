var cacheName = 'tokipona-v0.1.1';

var toCache = [
    '/logo.svg',
    '/logo-128x128.png',
    '/logo-256x256.png',
    '/logo-512x512.png',
    '/AutourOne-Regular.ttf',
    '/cookie.js',
    '/dictionary.js',
    '/app.js',
    '/flw.min.js',
    '/style.css',
    '/index.html'
];

self.addEventListener('install', (ev) => {
    console.log('SW - install');

    ev.waitUntil(
        caches.open(cacheName).then((cache) => {
            console.log('SW - caching files', toCache);
            return cache.addAll(toCache);
        })
    )
});

self.addEventListener('fetch', (ev) => {
    ev.respondWith(
        caches.match(ev.request).then((r) => {
            console.log('[Service Worker] Fetching resource: '+ev.request.url);
            return r || fetch(ev.request).then((response) => {
                return caches.open(cacheName).then((cache) => {
                console.log('[Service Worker] Caching new resource: '+ev.request.url);
                cache.put(ev.request, response.clone());
                return response;
            });
        });
    }));
});

self.addEventListener('activate', (e) => {
    e.waitUntil(
        caches.keys().then((keyList) => {
            return Promise.all(keyList.map((key) => {
            if(key !== cacheName) {
            return caches.delete(key);
            }
        }));
    }));
});