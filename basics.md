Toki Pona
=========

Toki Pona is a constructed language created by Canadian linguist and translator Sonja Lang in 2001. It's an experiment to see how minimalist a language can get.
This article describes the entire grammar of this language.
The article was inspired by [CCO ODT document](https://morr.cc/toki-pona-cheat-sheet/).

# Alphabet
Toki pona uses nine consonants (jklmnpstw) and five vowels (aeiou).
Pronounciation doesn't really matter.

# Basic sentences
The particle _li_ separates the subject and the verb:

_soweli li moku._ = The cat is eating.
_jan li lape._ = The person is sleeping.

There is no verb "to be"; the part after _li_ can also be a noun or an adjective.

_kili li moku._ = Fruits are food.
_telo li pona._ = Water is good.
_telo li moku._ = Water is a drink.

If the subject is _mi_ or _sina_, the _li_ is always omitted:

_mi moku._ = I eat.
_sina pona._ = You are good.

# Ambiguity
Toki Pona has a lot of ambiguity. You'll often need to know the context to decide what things mean. For example, verbs have no tense:

_mi moku._ = I am eating. / I was eating. /I will be eating.

Many words have multiple or general meanings:

_soweli_ = cat / dog / (any land mammal)
_kili_ = (any fruit ofr vegetable)

Many words can play the role of a noun, adjective, or verb:

_telo_ = water / wet / to wash
_pona_ = good, simple / to fix, to repair

Nouns have no singular/plural and no definitive/indefinitive article

_kili_ = a fruit / the fruit / some fruits / the fruits

# Direct objects
The particle _e_ separates a direct object from the rest of the sentence:

_soweli li moku e telo._ = The cat drinks the water.
_mi telo e soweli._ = I'm washing the cat.

# Modifying words
Words can be modified by appending other words:

_jan lili_ = small human, child
_tomo mi_ = my house
_pilin pona_ = to feel good, to be happy

# Negation
To negate a word, append _ala_:

_mi lape ala._ = I'm not sleeping.
_jan ala li toki._ = Nobody is talking.

# Questions
To ask yes/no questinos, replace the verb with "(verb) ala (verb)":

_sina ken ala ken lape?_ = Are you able to sleep?
_soweli li wile ala wile moku?_ Is the cat hungry?

Alternatively, append _anu seme_ ("or what") to the sentence:

_sina wile uta e mi anu seme?_ = Do you want to kiss me?

To answer these questions, reply with either "(verb)" or "(verb) ala".

To ask questions that can't be answered with yes or no, write a normal sentence and replace the word in question with _seme_:

_sina moku e seme?_ = What are you eating?
_seme li moku e kili mi?_ = Who/what ate my fruit?

# Modifying words using _pi_
To modify an expression with a group of words, separate them with the particle _pi_. It can often be thought of as "of". Note the difference:

_tomo telo nasa_ = crazy water house = strange bathrom
_tomo pi telo nasa_ = house of the crazy water = pub
_jan wawa ala_ = no strong people
_jan pi wawa ala_ = people of not-strong = weak people

# Providing context using _la_
To add a context to a sentence, prepend another sentence or expression, followed by _la_. This often results in a structure like "If (part 1) then (part 2)" or "in the context of (part 1), (part 2)."

_mi lape la ali li pona._ = When I'm asleep, everything is okay.

# Time
You can use a _la_-clause to add a temporal context to a sentence:

_tenpo ni la mi lape._ = I am sleeping right now.
_tenpo kama la mi lape._ = I will be sleeping in the future.
_tenpo pini la mi lape._ = I slept in the past.

# Compound sentences
Separate multiple subjects in a sentence using _en_:

_lape en moku li suli._ = Sleep and food are important.

To say that the subject dose more than one thing, you can use multiple _li_-clauses:

_pipi li moku li pakala._ = The bug eats and destroys.

If there are several direct objects of the same verb, you can use multiple _e_-clauses:

_mi moku e kili e telo._ = I consume fruit and water.

# Unofficial words
Unofficial words (like names of countries, languages, or people) are capitalized and treated like adjectives. They are attached to a nount, and often simplified to Toki POna's limited alphabet:

_mi jan Kapile._ = I'm Gabriele
_ma Kanata li pona lukin._ = Canada is pretty.
_mi toki ala e toki Inli._ = I don't speak English.
_ma tomo Nukoka li suli._= New York is big.

# Prepositions
_lon_, _kepeken_, _tawa_, and _tan_ can be used as prepositions at the end of a sentence:

_mi moku lon tomo._ = I eat in the house.
_mi moku kepeken ilo moku._ = I eat using a fork.
_sina pona tawa mi._ = You are good for me. = I like you.
_sina tawa weka tan seme?_ = Why are you leaving?

# Commands
To state a command, use _o_ and then what you want the person to do:

_o lukin e ni!_ = Look at this!

To address someone, start a sentence with "(person) o,":

_jan malin o, sina pona lukin._ = Malin, you are pretty.

You can also use this together with a command, merging the two _o_'s:

_jan San o tawa tomo sina._ = Sam, go home.

# Numbers
There were various attempts to standardize counting, but none of them was accepted yet. Until then, just combine the words to add them up:

_luka luka tu wan_ = 13 (luka = 5, tu = 2, wan = 1)

**And that's it!**