let dictionary = [
	{
		word: "a",
		synonym: "kin",
		usage: {
			particle: "(zdôraznenie, emócia alebo potvrdenie)"
		}
	},
	{
		word: "akesi",
		usage: {
			noun: "nie-milé zviera; plaz, obojživelník"
		}
	},
	{
		word: "ala",
		usage: {
			adjective: "nie, zápor, nula, nič"
		}
	},
	{
		word: "alasa",
		usage: {
			verb: "loviť, pásť"
		}
	},
	{
		word: "ale",
		synonym: "ali",
		usage: {
			adjective: "všetký; hojný, nespočetný, štedrý, každý",
			noun: "hojnosť, všetko, život, svet",
			number: "100"
		}
	},
	{
		word: "anpa",
		usage: {
			adjective: "klaňajúci, pokorný, klesajúci, prízemný, ponížený, závislý"
		}
	},
	{
		word: "ante",
		usage: {
			adjective: "iný, zmenený, upravený, ostatný"
		}
	},
	{
		word: "anu",
		usage: {
			particle: "alebo"
		}
	},
	{
		word: "awen",
		usage: {
			adjective: "trvalý, držaný, uchovávaný, chránený, bezpečený, zabezpečený, čakajúci, ostávajúci",
			preverb: "pokračujúci"
		}
	},
	{
		word: "e",
		usage: {
			particle: "(pred priamym objektom)"
		}
	},
	{
		word: "en",
		usage: {
			particle: "(medzi viacerými subjektami)"
		}
	},
	{
		word: "esun",
		usage: {
			noun: "obchod, trh, púť, bazár, obchodná transakcia, kšeft"
		}
	},
	{
		word: "ijo",
		usage: {
			noun: "vec, fenomén, objekt, hmota"
		}
	},
	{
		word: "ike",
		usage: {
			adjective: "zlý, negatívny; nepodstatný, nedôležitý"
		}
	},
	{
		word: "ilo",
		usage: {
			noun: "nástroj, implement, stroj, zariadenie, mašina"
		}
	},
	{
		word: "insa",
		usage: {
			noun: "centrum, obsah, vnútro, medzi; vnútorný orgán, brucho"
		}
	},
	{
		word: "jaki",
		usage: {
			adjective: "nechutný, obscénny, chorý, toxický, nečistý, znečistený, špinavý"
		}
	},
	{
		word: "jan",
		usage: {
			noun: "ľudská bytosť, osoba, niekto"
		}
	},
	{
		word: "jelo",
		usage: {
			adjective: "žltá, žltkastká"
		}
	},
	{
		word: "jo",
		usage: {
			verb: "mať, držať, obsahovať, držať"
		}
	},
	{
		word: "kala",
		usage: {
			noun: "ryba, morské zviera, morská príšera, tvor"
		}
	},
	{
		word: "kalama",
		usage: {
			verb: "produkovať zvuk, recitovať, vravieť nahlas, hovoriť"
		}
	},
	{
		word: "kama",
		usage: {
			adjective: "prichádzajúci, idúci, budúci, vyvolaný",
			preverb: "stať sa, dosiahnuť, uspieť (v)"
		}
	},
	{
		word: "kasi",
		usage: {
			noun: "rastlina, vegetácia; byliny, listy"
		}
	},
	{
		word: "ken",
		usage: {
			preverb: "byť schopný, mať umožnené, môcť, smieť",
			adjective: "možný, uskutočniteľný"
		}
	},
	{
		word: "kepeken",
		usage: {
			preposition: "použiť, s, so, pomocou"
		}
	},
	{
		word: "kili",
		usage: {
			noun: "ovocie, zelenina, huba"
		}
	},
	{
		word: "kiwen",
		usage: {
			noun: "tvrdý objekt, kov, kameň, skala"
		}
	},
	{
		word: "ko",
		usage: {
			noun: "hlina, prídružná forma, cesto, polotuhý, maz, prach"
		}
	},
	{
		word: "kon",
		usage: {
			noun: "vzduch, dych; esencia, duch; skrytá realita, neviditeľná entita"
		}
	},
	{
		word: "kule",
		usage: {
			adjective: "farebný, pigmentovaný, zafarbený, sfarbený"
		}
	},
	{
		word: "kulupu",
		usage: {
			noun: "komunita, spoločnosť, skupina, národ, spoločenstvo, kmeň"
		}
	},
	{
		word: "kute",
		usage: {
			noun: "ucho",
			verb: "počuť, počúvať; dávať pozor, poslúchať"
		}
	},
	{
		word: "la",
		usage: {
			particle: "(medzi kontextovou frázou a hlavnou vetou)"
		}
	},
	{
		word: "lape",
		usage: {
			adjective: "spiaci, odpočívajúci, oddychujúci"
		}
	},
	{
		word: "laso",
		usage: {
			adjective: "modrá, zelená"
		}
	},
	{
		word: "lawa",
		usage: {
			noun: "hlava, myseľ",
			verb: "ovládať, smerovať, viesť, vlastniť, plánovať, sprevádzať, regulovať, kontrolovať, vládnuť"
		}
	},
	{
		word: "len",
		usage: {
			noun: "látka, obledčenie, tkanina, textília, textil; obal, vrstva súkromia"
		}
	},
	{
		word: "lete",
		usage: {
			adjective: "chladný, studený; surový, nevarený, neuvarený"
		}
	},
	{
		word: "li",
		usage: {
			particle: "(medzi ľubovoľným subjektom okrem osamoteného `mi`, `sina` a ich slovies; tiež pre uvedenie nového slovesa k tomu istému subjektu)"
		}
	},
	{
		word: "lili",
		usage: {
			adjective: "malý, drobný, krátky; málo, kúsok, mladý"
		}
	},
	{
		word: "linja",
		usage: {
			noun: "dlhá a ohybná vec; kábel, vlas, lano, vlákno, priadza"
		}
	},
	{
		word: "lipu",
		usage: {
			noun: "plochý objekt; kniha, dokument, karta, papier, záznam, webstránka"
		}
	},
	{
		word: "loje",
		usage: {
			adjective: "červená, červenkastá"
		}
	},
	{
		word: "lon",
		usage: {
			preposition: "(umiestnený) v, (prítomný) vo, real, true, existing"
		}
	},
	{
		word: "luka",
		usage: {
			noun: "rameno, ruka, hmatový orgán",
			number: "päť"
		}
	},
	{
		word: "lukin",
		synonym: "oko",
		usage: {
			noun: "oko",
			verb: "pozerať, vidieť, skúmať, sledovať, čítať, pozerať, hľadieť",
			preverb: "to seek, look for, try to"
		}
	},
	{
		word: "lupa",
		usage: {
			noun: "door, hole, orifice, window"
		}
	},
	{
		word: "ma",
		usage: {
			noun: "earth, land; outdoors, world; country, territory; soil"
		}
	},
	{
		word: "mama",
		usage: {
			noun: "parent, ancestor; creator, originator; caretaker, sustainer"
		}
	},
	{
		word: "mani",
		usage: {
			noun: "money, cash, savings, wealth; large domesticated animal"
		}
	},
	{
		word: "meli",
		usage: {
			noun: "woman, female, feminine person; wife"
		}
	},
	{
		word: "mi",
		usage: {
			noun: "I, me, we, us"
		}
	},
	{
		word: "mije",
		usage: {
			noun: "man, male, masculine person; husband"
		}
	},
	{
		word: "moku",
		usage: {
			verb: "to eat, drink, consume, swallow, ingest"
		}
	},
	{
		word: "moli",
		usage: {
			adjective: "dead, dying"
		}
	},
	{
		word: "monsi",
		usage: {
			noun: "back, behind, rear"
		}
	},
	{
		word: "mu",
		usage: {
			particle: "(animal noise or communication)"
		}
	},
	{
		word: "mun",
		usage: {
			noun: "moon, night sky object, star"
		}
	},
	{
		word: "musi",
		usage: {
			adjective: "artistic, entertaining, frivolous, playful, recreational"
		}
	},
	{
		word: "mute",
		usage: {
			adjective: "many, a lot, more, much, several, very",
			noun: "quantity"
		}
	},
	{
		word: "nanpa",
		usage: {
			particle: "-th (ordinal number)",
			noun: "numbers"
		}
	},
	{
		word: "nasa",
		usage: {
			adjective: "unusual, strange; foolish, crazy; drunk, intoxicated"
		}
	},
	{
		word: "nasin",
		usage: {
			noun: "way, custom, doctrine, method, path, road"
		}
	},
	{
		word: "nena",
		usage: {
			noun: "bump, button, hill, mountain, nose, protuberance"
		}
	},
	{
		word: "ni",
		usage: {
			adjective: "that, this"
		}
	},
	{
		word: "nimi",
		usage: {
			noun: "name, word"
		}
	},
	{
		word: "noka",
		usage: {
			noun: "foot, leg, organ of locomotion; bottom, lower part"
		}
	},
	{
		word: "o",
		usage: {
			particle: "(hey! O! (vocative or imperative)"
		}
	},
	{
		word: "olin",
		usage: {
			verb: "to love, have compassion for, respect, show affection to"
		}
	},
	{
		word: "ona",
		usage: {
			noun: "he, she, it, they"
		}
	},
	{
		word: "open",
		usage: {
			verb: "to begin, start; open; turn on"
		}
	},
	{
		word: "pakala",
		usage: {
			adjective: "botched, broken, damaged, harmed, messed up"
		}
	},
	{
		word: "pali",
		usage: {
			verb: "to do, take action on, work on; build, make, prepare"
		}
	},
	{
		word: "palisa",
		usage: {
			noun: "long hard thing; branch, rod, stick"
		}
	},
	{
		word: "pan",
		usage: {
			noun: "cereal, grain; barley, corn, oat, rice, wheat; bread, pasta"
		}
	},
	{
		word: "pana",
		usage: {
			verb: "to give, send, emit, provide, put, release"
		}
	},
	{
		word: "pi",
		usage: {
			particle: "of"
		}
	},
	{
		word: "pilin",
		usage: {
			noun: "(heart (physical or emotional)",
			adjective: "feeling (an emotion, a direct experience)"
		}
	},
	{
		word: "pimeja",
		usage: {
			adjective: "black, dark, unlit"
		}
	},
	{
		word: "pini",
		usage: {
			adjective: "ago, completed, ended, finished, past"
		}
	},
	{
		word: "pipi",
		usage: {
			noun: "bug, insect, ant, spider"
		}
	},
	{
		word: "poka",
		usage: {
			noun: "hip, side; next to, nearby, vicinity"
		}
	},
	{
		word: "poki",
		usage: {
			noun: "container, bag, bowl, box, cup, cupboard, drawer, vessel"
		}
	},
	{
		word: "pona",
		usage: {
			adjective: "good, positive, useful; friendly, peaceful; simple"
		}
	},
	{
		word: "pu",
		usage: {
			adjective: "interacting with the official Toki Pona book"
		}
	},
	{
		word: "sama",
		usage: {
			adjective: "same, similar; each other; sibling, peer, fellow",
			preposition: "as, like"
		}
	},
	{
		word: "seli",
		usage: {
			adjective: "fire; cooking element, chemical reaction, heat source"
		}
	},
	{
		word: "selo",
		usage: {
			noun: "outer form, outer layer; bark, peel, shell, skin; boundary"
		}
	},
	{
		word: "seme",
		usage: {
			particle: "what? which?"
		}
	},
	{
		word: "sewi",
		usage: {
			noun: "area above, highest part, something elevated",
			adjective: "awe-inspiring, divine, sacred, supernatural"
		}
	},
	{
		word: "sijelo",
		usage: {
			noun: "(body (of person or animal), physical state, torso"
		}
	},
	{
		word: "sike",
		usage: {
			noun: "round or circular thing; ball, circle, cycle, sphere, wheel",
			adjective: "of one year"
		}
	},
	{
		word: "sin",
		synonym: "namako",
		usage: {
			adjective: "new, fresh; additional, another, extra"
		}
	},
	{
		word: "sina",
		usage: {
			noun: "you"
		}
	},
	{
		word: "sinpin",
		usage: {
			noun: "face, foremost, front, wall"
		}
	},
	{
		word: "sitelen",
		usage: {
			noun: "image, picture, representation, symbol, mark, writing"
		}
	},
	{
		word: "sona",
		usage: {
			verb: "to know, be skilled in, be wise about, have information on",
			preverb: "to know how to"
		}
	},
	{
		word: "soweli",
		usage: {
			noun: "animal, beast, land mammal"
		}
	},
	{
		word: "suli",
		usage: {
			adjective: "big, heavy, large, long, tall; important; adult"
		}
	},
	{
		word: "suno",
		usage: {
			noun: "sun; light, brightness, glow, radiance, shine; light source"
		}
	},
	{
		word: "supa",
		usage: {
			noun: "horizontal surface, thing to put or rest something on"
		}
	},
	{
		word: "suwi",
		usage: {
			adjective: "sweet, fragrant; cute, innocent, adorable"
		}
	},
	{
		word: "tan",
		usage: {
			preposition: "by, from, because of"
		}
	},
	{
		word: "taso",
		usage: {
			particle: "but, however",
			adjective: "only"
		}
	},
	{
		word: "tawa",
		usage: {
			preposition: "going to, toward; for; from the perspective of",
			adjective: "moving"
		}
	},
	{
		word: "telo",
		usage: {
			noun: "water, liquid, fluid, wet substance; beverage"
		}
	},
	{
		word: "tenpo",
		usage: {
			noun: "time, duration, moment, occasion, period, situation"
		}
	},
	{
		word: "toki",
		usage: {
			verb: "to communicate, say, speak, say, talk, use language, think"
		}
	},
	{
		word: "tomo",
		usage: {
			noun: "indoor space; building, home, house, room"
		}
	},
	{
		word: "tu",
		usage: {
			number: "two"
		}
	},
	{
		word: "unpa",
		usage: {
			verb: "to have sexual or marital relations with"
		}
	},
	{
		word: "uta",
		usage: {
			noun: "mouth, lips, oral cavity, jaw"
		}
	},
	{
		word: "utala",
		usage: {
			verb: "to battle, challenge, compete against, struggle against"
		}
	},
	{
		word: "walo",
		usage: {
			adjective: "white, whitish; light-coloured, pale"
		}
	},
	{
		word: "wan",
		usage: {
			adjective: "unique, united",
			number: "one"
		}
	},
	{
		word: "waso",
		usage: {
			noun: "bird, flying creature, winged animal"
		}
	},
	{
		word: "wawa",
		usage: {
			adjective: "strong, powerful; confident, sure; energetic, intense"
		}
	},
	{
		word: "weka",
		usage: {
			adjective: "absent, away, ignored"
		}
	},
	{
		word: "wile",
		usage: {
			preverb: "must, need, require, should, want, wish"
		}
	}
];

export {dictionary};