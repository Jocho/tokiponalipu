/**
 * Flw - wrap bindings faster
 *
 * Flw is a simple wrapper to handle 2-way databinding.
 *
 * @author Marek J. Kolcun
 * @version 0.1.13
 */

/**
 * Main Flw class.
 *
 * This class generates an instance of main object that handles
 * all the other flw-based elements.
 *
 * @public
 * @class
 */
class Flw {
    /**
     * A Flw constructor method.
     *
     * @param {string} rootSelector - an argument for `document.querySelector()`
     * @param {object} elems        - [opt] object-list of pre-generated Elems
     */
    constructor(rootSelector, elems) {
        this.root = document.querySelector(rootSelector);
        this.elems = new Map();
        elems = elems || {};

        for (let i in elems) {
            if (!elems.hasOwnProperty(i)) {
                continue;
            }
            this.addElem(elems[i], i);
        }
    }

    /**
     * Adds an Elem instance info list of handled objects.
     *
     * @public
     * @param {Elem} elem - an Elem class instance
     * @param {string} id - identifier
     */
    addElem(elem, id = null) {
        id = id || elem.getId();
        elem.setId(id);
        this.elems.set(id, elem);
    }

    /**
     * Removes an Elem instance from list of handled objects.
     *
     * @public
     * @param {string} id - Elem identifier
     */
    removeElem(id) {
        if (!this.hasElem(id))
            return;

        this.elems.delete(id);
        document.querySelector('[data-k="' + id + '"').remove();
    }

    /**
     * Checks whether an Elem instance with given `id` is already handled.
     *
     * @public
     * @param {string} id - Elem identifier
     * @return {bool}     - true if `id` is already registered
     */
    hasElem(id) {
        return this.elems.has(id);
    }

    /**
     * Main method that launches all the Flw functionality.
     *
     * @public
     */
    run() {
        while (this.root.childNodes.length) {
            this.root.firstChild.remove();
        }

        this.elems.forEach((x, y) => {
            let r = x.renderer.render();
            if (x.isElement) {
                r.dataset.id = y;
            }

            this.root.appendChild(r);
        });
    }
}

/**
 * Elem is a basic building block of Flw functionality.
 *
 * Every Elem object handles part of the application functionality, being it a
 * form, a login block, a shopping cart, a list of goods, etc.
 *
 * @public
 * @class
 */
class Elem {
    /**
     * An Elem constructor method.
     *
     * @param {string} templateName - it may be either a name of the template,
     *                                or a Text node content
     * @param {object} props        - object carrying all the props given to Elem
     */
    constructor(templateName, props = {}) {
        this.templateName = templateName;
        this.renderer = new Renderer(this);
        this.props = props;
        this.id = this.generateId(this.templateName);
        this.isElement = this.renderer.template.children.length
            && this.renderer.template.children[0] instanceof Element;
    }

    /**
     * Generates a random id with given prefix.
     *
     * @private
     * @param {string} prefix - basically any string
     */
    generateId(prefix) {
        return [
            prefix,
            (new Date()).getTime(),
            Math.floor(Math.random() * 1000)
        ].join('_');
    }

    /**
     * Sets the Elem's `id` identifier.
     *
     * @private
     * @param {string} id - new Elem id
     */
    setId(id) {
        this.id = id;
    }

    /**
     * Returns Elem's `id` identifier.
     *
     * @public
     * @return {string} - Elem's id
     */
    getId() {
        return this.id;
    }

    /**
     * Checks whether a value with given path exists in Elem props object.
     *
     * @param {*} path      - a dot-separated path to the props attribute
     * @param {object} data - used for recursive call
     * @return {bool}       - true if the path returns variable, false otherwise
     */
    has(path, data = null) {
        path = path instanceof Array ? path : path.split('.');
        data = data || this.props;

        let part = path.shift();

        if (typeof data[part] === 'undefined') {
            return false;
        }

        if (path.length) {
            return data[part] instanceof Elem
                ? data[part].get(path)
                : this.get(path, data[part]);
        }
        else {
            return true;
        }
    }

    /**
     * Returns props value by given path.
     *
     * @public
     * @param {string} path - a dot-separated path to the props attribute
     * @param {object} data - used for recursive call
     * @return {mixed}      - returns value of the targeted props attribute
     */
    get(path, data = null) {
        path = path instanceof Array ? path : path.split('.');
        data = data || this.props;

        let part = path.shift();

        if (typeof data[part] === 'undefined') {
            return null;
        }

        if (path.length) {
            return data[part] instanceof Elem
                ? data[part].get(path)
                : this.get(path, data[part]);
        }
        else {
            return data[part];
        }
    }

    /**
     * Sets the value into Elem's props by given path.
     *
     * @public
     * @param {string} path - dot-separated path to the props attribute; if the path
     *                        doesn't exist, it will be created.
     * @param {mixed} value - value to be stored in the props
     * @param {object} data - used for recursive call
     */
    set(path, value, data = null) {
        path = path instanceof Array ? path : path.split('.');
        data = data || this.props;

        let part = path.shift();

        if (path.length) {
            data[part] instanceof Elem
                ? data[part].set(path, value)
                : this.set(path, value, data[part] || {});
        }
        else {
            data[part] = value;
            this.renderer.update();
        }
    }
}

/**
 * A class that renders Elems into the DOM.
 *
 * Main class that handles Elem internal template and transforms it into the
 * HTML element that is then passed into DOM.
 *
 * @public
 * @class
 */
class Renderer {
    /**
     * An Elem constructor method.
     */
    constructor(elem) {
        this.elem = elem;
        this.template = this.findTemplate(elem.templateName);
    }

    /**
     * Tries to find a <template> defined in HTML by its name; if the template
     * is not found, a Text node with `templateName` as its content is created.
     *
     * @private
     * @param {string} templateName - name of the <template> to be found
     * @return {Element|Text}       - a node to be returned
     */
    findTemplate(templateName) {
        let template = document.querySelector('[name="' + templateName + '"]');
        if (template !== null) {
            return template.content.cloneNode(true)
        }
        else {
            let t = new Text();
            t.textContent = templateName;
            return t;
        }
    }

    /**
     * Generates a DOM interpretation of an Elem instance.
     *
     * @public
     * @param {Node} node - used for recursive call
     */
    render(node) {
        node = node || this.template.cloneNode(true);

        if (node instanceof DocumentFragment) {
            [...node.childNodes].forEach((x) => {
                x.replaceWith(this.render(x));
            });

        }
        else if (node instanceof Element) {
            // id
            if (typeof node.dataset.id !== 'undefined') {
                node.dataset.id = this.elem.getId();
            }

            // events
            if (typeof node.dataset.e !== 'undefined') {
                this.findAndInvokeEvents(node, node.dataset.e);
                delete (node.dataset.e);
            }
            // autobind form element events
            else {
                this.invokeAutoEvents(node);
            }

            // attributes
            [...node.attributes].forEach((x) => {
                if (x.name.search(/\{.+\}/) > -1) {
                    let y = this.findAndReplaceBindings(x.name).textContent.split('=');
                    node.removeAttribute(x.name);
                    node[y[0]] = y[1] || '"' + y[0] + '"';
                }
                else {
                    x.value = this.findAndReplaceBindings(x.valueOf().textContent).textContent;
                }
            });

            // children
            [...node.childNodes].forEach((x) => {
                x.replaceWith(this.render(x));
            });

            // update form element selection attribute
            if (typeof node.attributes.name !== 'undefined') {
                this.setFormInputSelectionAttribute(node);
            }
        }
        else if (node instanceof Node) {
            let newNode = this.findAndReplaceBindings(node.textContent);

            if (newNode instanceof DocumentFragment) {
                node.replaceWith(...newNode.childNodes);
            }
            else {
                node.replaceWith(newNode);
            }
        }

        return node instanceof DocumentFragment ? node.children[0] : node;
    }

    /**
     * Used when an existing DOM interpretation of Elem should be updated.
     *
     * When none attribute is provided, default values from existing DOM and
     * Elem's template are generated; then both Elements are traversed and
     * existing DOM node is updated based on the values of generated Node.
     *
     * @public
     * @param {Node} temp - an existing Node in DOM
     * @param {Node} rend - a generated Node that serves as template
     */
    update(temp, rend) {
        temp = temp || document.querySelector('[data-id="' + this.elem.id + '"]');
        rend = rend || this.render();

        if (temp === null) {
            temp = rend;
        }
        else if (temp instanceof Text && temp.textContent == '') {
            temp.replaceWith(rend);
        }
        else {
            if (temp instanceof Element) {
                // remove old attributes
                for (let i in temp.attributes) {
                    if (!temp.attributes.hasOwnProperty(i))
                        continue;

                    let found = false;

                    for (let j in rend.attributes) {
                        if (!rend.attributes.hasOwnProperty(j))
                            continue;

                        if (rend.attributes[j].name == temp.attributes[i].name) {
                            found = true;
                            break;
                        }
                    }

                    if (!found) {
                        temp.removeAttribute(temp.attributes[i].name);
                    }
                }

                // update existing attributes
                for (let i in rend.attributes) {
                    if (!rend.attributes.hasOwnProperty(i)
                        || temp instanceof Text
                        || (typeof temp.attributes[i] !== 'undefined'
                            && temp.attributes[i].value == rend.attributes[i].value)
                    ) {
                        continue;
                    }

                    temp.setAttribute(
                        rend.attributes[i].name,
                        rend.attributes[i].value
                    );
                }

                // update form element selection attribute
                if (typeof temp.attributes.name !== 'undefined') {
                    this.setFormInputSelectionAttribute(temp, rend);
                }
            }

            // handle child nodes based on their amount
            if (
                !rend.childNodes.length
                && temp.textContent != rend.textContent
            ) {
                temp.replaceWith(rend);
            }
            else if (temp.childNodes.length <= rend.childNodes.length) {
                [...rend.childNodes].forEach((x, i) => {
                    if (typeof temp.childNodes[i] === 'undefined') {
                        temp.appendChild(new Text());
                    }

                    this.update(temp.childNodes[i], x);
                });
            }
            else {
                let cnt = rend.childNodes.length;
                let nodesMoved = 0;

                [...temp.childNodes].forEach((x, y, z) => {
                    if (y < cnt) {
                        x.replaceWith(rend.childNodes[y - nodesMoved])
                        nodesMoved++;
                    }
                    else {
                        x.remove();
                    }
                });
            }
        }
    }

    /**
     * Checkes whether the Element node is select|checkbox|radio button
     * and sets appropriate attribute if its value is set in the data..
     *
     * @param {Element} node - previous node element
     * @param {Element} rend - rendered node element
     */
    setFormInputSelectionAttribute(node, rend) {
        if (!this.elem.has(node.attributes.name.value))
            return;

        let values = this.elem.get(node.attributes.name.value);

        if (node.tagName == 'SELECT') {
            if (!(values instanceof Array)) {
                values = values != null ? [values] : [];
            }

            [...node.options].forEach((x) => {
                x.selected = false;
                for (let i = 0; i < values.length; i++) {
                    if (values[i] == x.value) {
                        x.selected = true;
                        x.setAttribute('selected', 'selected');
                        break;
                    }
                }
            });
        }
        else if (
            node.tagName == 'INPUT'
            && typeof node.attributes.type !== 'undefined'
            && ['checkbox', 'radio'].indexOf(node.attributes.type.value) > -1
        ) {
            node.checked = false;
            if (values instanceof Array) {
                for (let i = 0; i < values.length; i++) {
                    if (values[i] == node.value) {
                        node.checked = true;
                        node.setAttribute('checked', 'checked');
                        break;
                    }
                }
            }
            else if (values == node.value) {
                node.checked = true;
            }
        }
        else if (typeof rend !== 'undefined'
            && rend.tagName == 'INPUT'
            && node.value != rend.value
        ) {
            node.value = rend.value;
        }
    }

    /**
     * Generates default event listener on the form inputs.
     *
     * When an element's `tagName` is found in the switch-case, an event
     * listener with corresponding event is generated to store element's value
     * into Elem's props when the value changes. The event is generated only
     * for elements that carry a `name` attribute; those without it are ignored.
     *
     * @private
     * @param {Element} node - an element to be checked and "eventualized"
     */
    invokeAutoEvents(node) {
        let event = null;

        if (typeof node.attributes.name === 'undefined')
            return;

        switch (node.tagName) {
            case 'INPUT':
                switch (node.attributes.type.value) {
                    case 'text':
                    case 'number':
                    case 'phone':
                    case 'email':
                    case 'tel':
                    case 'url':
                    case 'range':
                    case 'color':
                        event = 'input';
                        break;

                    case 'radio':
                    case 'checkbox':
                    default:
                        event = 'change';
                        break;
                }
                break;
            case 'SELECT':
                event = 'change';
                break;

            case 'TEXTAREA':
                event = 'input';
                break;

            case 'BUTTON':
                event = 'click';
                break;

            default:
                return;
        }

        if (event == null)
            return;

        node.addEventListener(event, (ev) => {
            ((ev, me) => {
                let value;

                if (
                    ev.target.tagName == 'INPUT'
                    && ev.target.type == 'checkbox'
                ) {
                    value = this.elem.get(ev.target.name) || [];
                    value = (value instanceof Array) ? value : [value];

                    if (ev.target.checked) {
                        value.push(ev.target.value);
                    }
                    else {
                        value.splice(value.indexOf(ev.target.value), 1);
                    }
                }
                else if (
                    ev.target.tagName == 'SELECT'
                    && ev.target.hasAttribute('multiple')
                ) {
                    value = [];
                    [...ev.target.selectedOptions].forEach((x) => {
                        value.push(x.value);
                    });
                }
                else {
                    value = ev.target.value;
                }

                this.elem.set(ev.target.name, value);
            })(ev, this.elem);
        });
    }

    /**
     * Sets event listeners to given element.
     *
     * If an element carries a `data-e` attribute ("e" stands for "event"),
     * the content of the `dataset.e` is parsed. There can be multiple callbacks
     * defined, separated by space. Each callback is defined by listener event
     * and function name in following format:
     *
     * event:function
     *
     * The listener event is default event of `addEventListener()` function and
     * function name is a path to the Elem's props attribute.
     *
     * Generated function has 2 arguments - 1st argument is default target event
     * and 2nd argument is Elem itself.
     *
     * @private
     * @param {Element} element - DOM element to be enriched by events
     * @param {string} datasetE - dataset.e to be parsed
     */
    findAndInvokeEvents(element, datasetE) {
        let matches = datasetE.matchAll(/([a-z\d_|]+?):([a-z\d_.]+)/gi);
        let match;

        do {
            match = matches.next();

            if (typeof match.value === 'undefined')
                continue;

            let event = this.elem.get(match.value[2]);

            if (typeof event === 'function') {
                match.value[1].split('|').forEach((x) => {
                    element.addEventListener(
                        x,
                        (ev) => { event(ev, this.elem) },
                        false
                    );
                });
            }
            else {
                console.error(
                    'Could not invoke `'
                    + match.value[1]
                    + '` listener, `'
                    + match.value[2]
                    + '` is not a function.'
                );
            }

        }
        while (!match.done);
    }

    /**
     * Returns DocumentFragment or Node with content generated by Elem's props.
     *
     * If an Elem's template carries a {variable} in any attribute or in
     * textContent itself, it is replaced by corresponding Elem's props value.
     * The variable is passed into Elem.get() method and based on its
     * instance it is transfered either into DocumentFragment or Node.
     *
     * @private
     * @param {Node} strNodeValue - string of a node containing {variable}
     */
    findAndReplaceBindings(strNodeValue) {
        let match;
        let matches = strNodeValue.matchAll(/\{(.+?)\}/g);
        let tempFragment = document.createDocumentFragment();

        do {
            match = matches.next();
            if (typeof match.value !== 'undefined') {
                let matchIndex = strNodeValue.indexOf(match.value[0]);
                let clearText = strNodeValue.substr(0, matchIndex);

                if (clearText.length) {
                    tempFragment.appendChild(new Text(clearText));
                }

                strNodeValue = strNodeValue.substr(matchIndex + match.value[0].length);

                let propsValue = this.elem.get(match.value[1]);
                let nv = this.nodizeValue(propsValue);

                if (nv instanceof DocumentFragment) {
                    if (nv.childNodes.length) {
                        tempFragment.append(...nv.childNodes);
                    }
                    else {
                        tempFragment.appendChild(new Text());
                    }
                }
                else {
                    // console.log(propsValue, nv);
                    tempFragment.appendChild(nv);
                }
            }
            else {
                tempFragment.appendChild(new Text(strNodeValue));
            }
        }
        while (!match.done);

        return (tempFragment.childNodes.length)
            ? tempFragment
            : strNodeValue;
    }

    /**
     * Translates given value into a Node.
     *
     * Takes any value and, based on its instance, translates it either into
     * Element or a Text Node.
     *
     * @param {mixed} propsValue - node string value translated into Node
     * @return {Text|DocumentFragment}
     */
    nodizeValue(propsValue) {
        if (propsValue instanceof Array) {
            let fragment = document.createDocumentFragment();
            propsValue.forEach((x) => {
                let vls = this.nodizeValue(x);

                if (vls instanceof DocumentFragment) {
                    fragment.append(...vls.childNodes);
                }
                else {
                    fragment.appendChild(vls);
                }
            });
            return fragment;
        }

        if (propsValue instanceof Elem) {
            let x = propsValue.renderer.render();
            return x;
        }

        if (typeof propsValue !== 'object') {
            return new Text(propsValue);
        }

        if (propsValue == null) {
            return new Text();
        }

        return new Text(JSON.stringify(propsValue));
    }
}
